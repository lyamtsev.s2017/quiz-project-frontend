const colors = require('tailwindcss/colors')

module.exports = {
  purge: { content: ['./public/**/*.html', './src/**/*.vue'] },
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      black: {
        DEFAULT: '#111C31',
        dark: '#222222'
      },
      white: colors.white,
      primary: {
        DEFAULT: '#F9F8F9'
      },
      red: {
        DEFAULT: '#C32026',
        darker: '#AF1C22',
        dark: '#9C191E',
        light: '#CC0000',
      },
      grey: {
        light: '#E8E8E8',
        DEFAULT: '#C4C4C4',
        darker: '#E2E2E2',
        dark: '#717171'
      }
    },
    fontFamily: {
      'sans': ['Campton', 'sans-serif'],
    },
    boxShadow: {
      input: '0 2px 8px 0 rgba(0, 0, 0, 0.04)',
      'input-hover-pressed': '0 2px 8px 0 rgba(0, 0, 0, 0.08)',
    },
    extend: {
      gridTemplateColumns: {
        input: '24px 1fr',
        navbar: '1fr minmax(1fr, 500px) 1fr',
        'mp-content': '1fr 300px',
        payment: '16px 2fr 1fr 1fr'
      },
      backgroundImage: theme => ({
        checkmark: "url('~@/assets/img/icons/tick.svg')",
      }),
      maxWidth: {
        quiz: '500px',
      },
      width: {
        '300px': 300,
      },
      height: {
        '300px': 300
      },
      fontFamily: {
        'pt-sans': ['PT Sans', 'sans-serif'],
      }
    },
  },
  variants: {
    extend: {
      borderWidth: ['active', 'hover'],
      cursor: ['disabled'],
      opacity: ['disabled'],
      backgroundColor: ['active', 'disabled', 'group-focus'],
      color: ['group-focus'],
    },
  },
  plugins: [
    require('tailwindcss'),
    require('autoprefixer'),
  ],
}
