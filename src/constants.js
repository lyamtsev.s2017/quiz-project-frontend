const SILDENAFIL_FULL_NAME = 'Generic Viagra (Sildenafil)'
const TADALAFIL_FULL_NAME = 'Generic Cialis (Tadalafil)'

export const constants = {
  ANSWERS_KEYS: {
    // QUIZZES
    GET_STARTED: 'GET_STARTED',
    WHAT_RESULTS: 'WHAT_RESULTS',
    THE_BASICS: 'THE_BASICS',
    YOUR_HEALTH: 'YOUR_HEALTH',
    TREATMENT_PREFERENCE: 'TREATMENT_PREFERENCE',
    VISIT_SUMMARY: 'VISIT_SUMMARY',
    CHECKOUT: 'CHECKOUT',
    // MISC
    SMS_UPDATES: 'SMS_UPDATES',
    OFFER_MODAL: 'OFFER_MODAL'
  },
  NONE_CHOICE: 'None of the above',
  YES_CHOICE: 'Yes',
  SILDENAFIL_FULL_NAME,
  TADALAFIL_FULL_NAME,
  EVERY_3_MONTHS: 'Ship every 3 months',
  EVERY_MONTH: 'Ship every month',
  SHORT_TABLET_NAME: {
    [SILDENAFIL_FULL_NAME]: 'Sildenafil',
    [TADALAFIL_FULL_NAME]: 'Tadalafil',
  }
}
