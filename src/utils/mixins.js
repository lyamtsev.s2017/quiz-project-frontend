import {mapGetters, mapMutations} from "vuex";
import {constants} from "@/constants";

export const checkboxMixin = {
  data() {
    return {
      checkboxSelected: [],
      checkHandler: () => {throw 'No handler specified for checkbox'}
    }
  },
  mounted() {
    this.$watch('checkboxSelected', this.checkHandler, {
      deep: true
    })
  },
  methods: {
    handleCheckboxInput(val) {
      if (this.checkboxSelected.includes(val)) {
        const idx = this.checkboxSelected.findIndex(item => item === val)
        this.checkboxSelected.splice(idx, 1)
      } else {
        this.checkboxSelected.push(val)
      }
    },
    // Used for choices with NONE_CHOICE
    filterChoices(newVal) {
      if (newVal.length > 1 && newVal.includes(constants.NONE_CHOICE)) {
        const idx = newVal.indexOf(constants.NONE_CHOICE)
        if (idx === 0) {
          newVal.splice(idx, 1)
        } else {
          newVal = [constants.NONE_CHOICE]
        }
      }

      return newVal
    }
  }
}

export const answersMixin = {
  methods: {
    ...mapMutations([
      'SET_ANSWERS',
      'SET_ANSWER',
    ]),
    setAnswer(answerKey, value, valueKey) {
      this.SET_ANSWER({answerKey, value, valueKey})
    }
  },
  computed: {
    ...mapGetters([
      'answers',
      'answer',
    ]),
    answersKeys() {
      return constants.ANSWERS_KEYS
    },
  }
}

export const productMixin = {
  // REQUIRES AN ANSWER MIXIN
  computed: {
    boughtProduct() {
      const answer = this.answer(this.answersKeys.TREATMENT_PREFERENCE)
      const additionalData = answer?.how_often === constants.EVERY_3_MONTHS ? {
        oldPrice: 180,
        newPrice: 153
      } : {
        price: 60
      }
      return {
        name: answer?.what_type || 'ERR',
        supplyDuration: answer?.how_often === constants.EVERY_3_MONTHS ? '3 month supply' : '1 month supply',
        shipping: answer?.how_often === constants.EVERY_3_MONTHS ? 'Ships every 3 months' : 'Ship once',
        ...additionalData
      }
    },
    orderedItems() {
      return this.answer(this.answersKeys.VISIT_SUMMARY)?.ordered_items || []
    }
  },
}

export const referMixin = {
  computed: {
    ...mapGetters([
      'refer'
    ])
  },
  methods: {
    ...mapMutations([
      'SET_REFER'
    ]),
    setRefer(title, info) {
      this.SET_REFER({title, info})
    },
    clearRefer() {
      this.SET_REFER(null)
    }
  }
}
