import Vue from 'vue'
import VueRouter from 'vue-router'
import PageQuiz from "@/views/PageQuiz";
import MainPage from "@/views/MainPage";

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    name: 'main',
    component: MainPage
  },
  {
    path: '/quiz',
    name: 'quiz',
    component: PageQuiz
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
